/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f4xx.h"
#include "stm32f411e_discovery.h"

#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "usbd_desc.h"

#include "ring_buf.h"

static uint32_t Demo_USBConfig(void);
static void SystemClock_Config(void);
static void Error_Handler(void);

static rb_struct rb;

int main(void)
{

	SystemClock_Config();

	HAL_Init();

	Demo_USBConfig();

	for(;;);
}

/**
  * @brief  Initializes the USB for the demonstration application.
  * @param  None
  * @retval None
  */
static uint32_t Demo_USBConfig(void)
{
  /* Init Device Library */
  USBD_Init(&hUSBDDevice, &CDC_Desc, 0);

  /* Add Supported Class */
  USBD_RegisterClass(&hUSBDDevice, USBD_CDC_CLASS);

  /* Register user interface */
  USBD_CDC_RegisterInterface(&hUSBDDevice, &USBD_CDC_LSE_fops);

  /* Start Device Process */
  USBD_Start(&hUSBDDevice);

  /* Ready for reception */
  USBD_CDC_ReceivePacket(&hUSBDDevice);

  return 0;
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* Turn LED4 on */
  BSP_LED_On(LED4);
  while(1)
  {
  }
}

static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is
     clocked below the maximum system frequency, to update the voltage scaling value
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
	  Error_Handler();
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
	  Error_Handler();
  }
}

void HAL_SYSTICK_Callback(void)
{
	static int count = 0;
	uint16_t size;
	static unsigned char buf[] = "tx0: RA000\r\n";

	if (count >= 1000) {
		count = 0;
		size = rb_len(&rb);
		if (size > 0) {
			while (size && rb_read(&rb, 0) != 'R') {
				rb_pop(&rb);
				size--;
			}
		}
		if (size > 5) {
			uint8_t i;
			for (i=0; i<5; i++) {
				buf[i+5] = rb_pop(&rb);
			}
			size -= 5;
			while (size && (rb_pop(&rb) != '\r')) {
				size--;
			}

			if (USBD_CDC_SetTxBuffer(&hUSBDDevice, buf, sizeof(buf)-1)==USBD_OK) {
				if (USBD_CDC_TransmitPacket(&hUSBDDevice)==USBD_OK) {
					buf[2]++;
					if (buf[2] > '9') {
						buf[2] = '0';
					}
				}
			}
		}
	} else {
		count++;
	}
}

void LSE_Receive_callback(uint8_t* Buf, uint32_t Len) {
	while(Len--) {
		rb_write(&rb, *Buf++);
	}
}
