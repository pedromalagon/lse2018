/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f4xx.h"
#include "stm32f411e_discovery.h"

/* Variables used for timer */
uint16_t PrescalerValue = 0;
TIM_HandleTypeDef htim4;
TIM_OC_InitTypeDef sConfigTim4;

static void Error_Handler(void)
{
  /* Turn LED5 on */
  BSP_LED_On(LED5);
  while(1)
  {
  }
}

static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is
     clocked below the maximum system frequency, to update the voltage scaling value
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /* Enable HSI Oscillator and activate PLL with HSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 0x10;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 384;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 8;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

static void TIM4_Config(void)
{
  /* -----------------------------------------------------------------------
  TIM4 Configuration: Output Compare Timing Mode:

  In this example TIM4 input clock (TIM4CLK) is set to 2 * APB1 clock (PCLK1),
  since APB1 prescaler is different from 1 (APB1 Prescaler = 4, see system_stm32f4xx.c file).
  TIM4CLK = 2 * PCLK1
  PCLK1 = HCLK / 4
  => TIM4CLK = 2*(HCLK / 4) = HCLK/2 = SystemCoreClock/2

  To get TIM4 counter clock at 2 KHz, the prescaler is computed as follows:
  Prescaler = (TIM4CLK / TIM4 counter clock) - 1
  Prescaler = (84 MHz/(2 * 2 KHz)) - 1 = 41999

  To get TIM4 output clock at 1 Hz, the period (ARR)) is computed as follows:
  ARR = (TIM4 counter clock / TIM4 output clock) - 1
  = 1999

  TIM4 Channel1 duty cycle = (TIM4_CCR1/ TIM4_ARR)* 100 = 50%
  TIM4 Channel2 duty cycle = (TIM4_CCR2/ TIM4_ARR)* 100 = 50%
  TIM4 Channel3 duty cycle = (TIM4_CCR3/ TIM4_ARR)* 100 = 50%
  TIM4 Channel4 duty cycle = (TIM4_CCR4/ TIM4_ARR)* 100 = 50%

  ==> TIM4_CCRx = TIM4_ARR/2 = 1000  (where x = 1, 2, 3 and 4).
  ----------------------------------------------------------------------- */

  /* Compute the prescaler value */
  PrescalerValue = (uint16_t) (SystemCoreClock / 180000) - 1;

  /* Time base configuration */
  htim4.Instance             = TIM4;
  htim4.Init.Period          = 3600;
  htim4.Init.Prescaler       = PrescalerValue;
  htim4.Init.ClockDivision   = 0;
  htim4.Init.CounterMode     = TIM_COUNTERMODE_UP;
  if(HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /* TIM PWM1 Mode configuration: Channel */
  /* Output Compare Timing Mode configuration: Channel1 */
  sConfigTim4.OCMode = TIM_OCMODE_PWM1;
  sConfigTim4.OCIdleState = TIM_CCx_ENABLE;
  sConfigTim4.Pulse = 270;
  sConfigTim4.OCPolarity = TIM_OCPOLARITY_HIGH;

  /* Output Compare PWM1 Mode configuration: Channel1 */
  if(HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_1) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /* Output Compare PWM1 Mode configuration: Channel2 */
  if(HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_2) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /* Output Compare PWM1 Mode configuration: Channel3 */
  if(HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_3) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
  /* Output Compare PWM1 Mode configuration: Channel4 */
  if(HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigTim4, TIM_CHANNEL_4) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
}


int main(void)
{
	SystemClock_Config();

	HAL_Init();

	BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);
	BSP_LED_Init(LED5);
	BSP_LED_Init(LED6);

	TIM4_Config();
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);

	for(;;) {
		HAL_Delay(500);
		sConfigTim4.Pulse++;
		if (sConfigTim4.Pulse > 360) {
			sConfigTim4.Pulse = 180;
		}

		//HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_1);
        //__HAL_TIM_SET_COUNTER(&htim4,0);
		__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, sConfigTim4.Pulse);
		HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);

		//BSP_LED_Toggle(LED3);
	}
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  /* --------------------------- System Clocks Configuration -----------------*/
  /* TIM4 clock enable */
  __HAL_RCC_TIM4_CLK_ENABLE();

  /* GPIOD clock enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*-------------------------- GPIO Configuration ----------------------------*/
  /* GPIOD Configuration: Pins 12, 13, 14 and 15 in output push-pull */
  GPIO_InitStructure.Pin = GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
  GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStructure.Pull = GPIO_NOPULL;
  GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
  GPIO_InitStructure.Alternate = GPIO_AF2_TIM4;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStructure);
}

/**
  * @brief  DeInitializes the TIM PWM MSP.
  * @param  htim: TIM handle
  * @retval None
  */
void HAL_TIM_PWM_MspDeInit(TIM_HandleTypeDef *htim)
{
  /* TIM4 clock reset */
  __HAL_RCC_TIM4_FORCE_RESET();
  __HAL_RCC_TIM4_RELEASE_RESET();
}

