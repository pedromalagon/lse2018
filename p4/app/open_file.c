#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main () {
  char data[] = "Hola";
  int fd = open("/tmp/test", O_RDWR);
  if (fd < 0) {
    printf("Create the file. touch /tmp/test\n");
    return -1;
  }

  int ret = write(fd, data, strlen(data));
  printf("%d bytes written\n", ret);

  close(fd);

  return 0;
}
